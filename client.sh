#!/bin/sh

VBoxManage createvm \
   --name pxent \
   --basefolder "$PWD" \
   --ostype Linux_64 \
   --register
 
VBoxManage modifyvm pxent \
   --memory 68 \
   --vram 8 \
   --cpus 1 \
   --boot1 net \
   --boot2 none \
   --boot3 none \
   --boot4 none \
   --nic1 hostonly \
   --hostonlyadapter1 "${IFNAME:-vboxnet3}"

VBoxManage startvm pxent --type gui
