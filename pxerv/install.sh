#!/bin/sh

IP="`ip a show eth1 | grep -o '[0-9]*\.[0-9.]*'`"
PREFIX="${IP%.*}"
START="$PREFIX.2"
END="$PREFIX.5"

expand_it() {
   sed -e "s/\$IP/$IP/g;s/\$START/$START/g;s/\$END/$END/g"
}

for repo in `cat MIRRORS.txt` ; do
   echo "$repo/v3.3/main" | sed -e 's#//#/#g;s#:/#://#' > /etc/apk/repositories
   apk update && break
done

apk --update add tftp-hpa lighttpd build-base perl cdrkit git xz-dev tcpdump

cp initrd.img vmlinuz-3.16.0-rc4 /var/www/localhost/htdocs/
for f in http*.ipxe ; do
   expand_it <"$f" >"/var/www/localhost/htdocs/$f"
done

expand_it <udhcpd.conf >/etc/udhcpd.conf

git clone git://git.ipxe.org/ipxe.git
expand_it <origin.ipxe >ipxe/src/origin.ipxe
(
   cd ipxe/src
   make bin/undionly.kpxe EMBED=origin.ipxe
   cp bin/undionly.kpxe /var/tftpboot/ipxe
)


for service in in.tftpd udhcpd lighttpd ; do
   rc-update add "$service"
   rc-service "$service" restart
done
