#!/bin/bash
IFNAME="$(VBoxManage hostonlyif create | sed -e "s/.*'\(.*\)'.*/\1/")"
VBoxManage hostonlyif ipconfig "$IFNAME" --ip 192.168.76.1 --netmask 255.255.255.248
echo "export IFNAME=$IFNAME"
