## iPXE demo/proof of concept

This repository contains all components for a small demo about [iPXE][], to be
executed in [VirtualBox][] (with the help of [Vagrant][], too).

*Note: this repository contains two files ([initrd.img][] and
[vmlinuz-3.16.0-rc4][]) taken from the original [iPXE][] example as found at
[http://boot.ipxe.org/demo/][ipxe-boot-example].*

### What's this about?

I wanted to try out [iPXE][] in some non-trivial setup, i.e. using a real server
with DHCP and TFTP and a web server too (because [iPXE][] supports that too). So
I turned to [VirtualBox][] for setting up a demo server and a minimal client and
try [iPXE][] out.

The demo includes:

- set up of a small server, which acts as DHCP server to provide addresses to
the diskless clients, as well as TFTP server for initial download of [iPXE][]
and as web server for the following interactions;

- create a test client with very minimal requirements and no disk, executing a
network boot and eventually getting started thanks to the server above.

### Tools and Assumptions

The demo is executed on virtual machines using [VirtualBox][]. This is open
source and easy to install in a lot of different places.

The server and the client will both be attached on a network that is restricted
to the host. This is obtained using [Host-only networking][] and in particular
assuming the following:

- the network `192.168.76.1/29` is available for *host-only* networking;
- it is possible to create a new *host-only* network (there is a limitation on
how many you can have in Linux and Mac, although the limit is high).

The following sub-sections contain more details and assumption for each component.

#### Network

The script `network.sh` helps automating the creation of the *host-only*
network. At the end of the execution it prints out a shell command that you
should run in order to pass the right value to the client script, later.

Example:

    $ ./network.sh
    0%...10%...20%...30%...40%...50%...60%...70%...80%...90%...100%
    export IFNAME=vboxnet3

    $ export IFNAME=vboxnet3

    $ echo "$IFNAME"
    vboxnet3


#### Server

To ease the creation of the *server* machine I used [Vagrant][], defining the
details inside the `Vagrantfile`.

The VM for the Server has two network interfaces, one for communication with
the host (through which we will perform the installation of packages and
deposit our files for the demo) and the other one for the demo itself (i.e.
communication with the client or clients).

The address on the first interface is retrieved via DHCP from the host so there
is no explicit configurtion.

The address on the second interface is statically set to `192.168.76.6`, which
is the last address in the *host-only* network that has been created.

The starting OS image is based on [Alpine Linux][] to help reduce the size of
the demo. As a consequence, we will use the following softwares:

- [udhcpd][], a DHCP server (and client) which comes embedded with [Busybox][]
in Alpine's latest releases. This allows serving DHCP/BOOTP requests coming
from clients trying to boot from the network via [PXE][]; 

- [tftp-hpa][], for delivering the initial [iPXE][] image to the clients
asking for one during [PXE][] boot;

- [lighttpd], a HTTP server for delivering following requests (hence allowing
to try out [iPXE][] features).

- packages for cloning [iPXE][]'s repository and compile it

- [tcpdump], in case we want to take a look at what goes on in the servers's
`eth1` network interface (the one where it communicates with clients).

#### Client

The client is as simple as it can be. The `client.sh` script allows to create
it in [VirtualBox][], configure for [PXE][] boot and start it.

Memory is set to 68 MB because it's the lowest value that allows doing a
`poweroff` that also closes the window. At least in my tests. These tests also
showed that you can go as low as 36 MB of memory, but not less than this.

### Demo Time!

We will not describe the whole demo in detail here, because you can understand
all the details looking into the relevant scripts `network.sh`, `server.sh` and
`client.sh`.

First of all we create the networking for *host-only* communications:

    $ ./network.sh
    0%...10%...20%...30%...40%...50%...60%...70%...80%...90%...100%
    export IFNAME=vboxnet3

    $ export IFNAME=vboxnet3

    $ echo "$IFNAME"
    vboxnet3

Then, we start the server. It's important to start it *after* the network has
been created, otherwise [Vagrant][] will create one for us with different
parameters.

    $ ./server.sh
    ...
    lots of lines
    ...

What happens in this phase is:

- the server is created (through [Vagrant][])
- the contents of sub-directory `pxerv` are uploaded in the VM
- the `pxerv/install.sh` initialization script is executed

The last step takes care to install all needed packages in the server VM,
configure them, put the relevant files in the relevant places, and also
download and compile [iPXE][] on the spot. In this way, you will also be
left with a working environment for doing your experiments.

When the server is ready, you should see something like this:

      ...
      [FINISH] bin/undionly.kpxe
    rm bin/version.undionly.kpxe.o bin/undionly.kpxe.zinfo bin/undionly.kpxe.bin bin/undionly.kpxe.zbin
     * service in.tftpd added to runlevel default
     * Caching service dependencies ... [ ok ]
     * Starting tftpd ... [ ok ]
     * service udhcpd added to runlevel default
     * Starting busybox udhcpd ... [ ok ]
     * service lighttpd added to runlevel default
     * Starting lighttpd ... [ ok ]

The `FINISH` part about `undionly.kpxe` tells us that the compilation of
[iPXE][] completed successfully, the following lines that the different
services started cleanly.

If you are curious about the traffic, you can now log into the server
VM and start snooping the traffic on `eth1`:

    $ vagrant ssh # logs into the VM

    alpine-331:~$ tcpdump -i eth1

In the host machine, we're now ready to start our client:

    $ ./client.sh

This takes care to create a new VM, register it into [VirtualBox][] and then
start it. If all is working fine, this is what happens:

- the client machine sends a DHCP/BOOTP request on the network it is attached
to, which is also where `eth1` in the server is connected;

- the server responds to the DHCP request sending the details about booting.
You can find these details in file `/etc/udhcpd.conf` in the server machine;

- the client downloads the initial [iPXE][] boot program from the server via
TFTP, and runs it.

At this point, [iPXE][] is in charge. The specific demo does the following:

- the initial file that is provided (embedded with the initial boot program)
is `pxerv/http.ipxe` (`$IP` is expanded with the server's IP address during
the installation process in the server):

        #!ipxe
        chain http://$IP/http-prompt.ipxe

- the file `pxserv/http-prompt-ipxe` is retrieved via HTTP. It prints out a
message and waits for you to press a key before going on:

        #!ipxe
        prompt Press any key to continue
        chain http://$IP/http-boot.ipxe

- after you press the key, it goes on downloading `pxerv/http-boot.ipxe`, which
does boot a small Linux instance at last:

        #!ipxe
        kernel vmlinuz-3.16.0-rc4 fastboot initrd=initrd.img
        initrd initrd.img
        boot

At this point, you can go in the client's console and try out a few commands,
e.g `ls -l`. Yay! Don't forget to `poweroff` when you're done!

### Customize

If you want to play around, you might want to know that...

- networking is sort-of hardcoded in a few places:
   - `Vagrantfile`
   - `network.sh`
   - `client.sh`: this needs the environment variable `IFNAME` to be set, just
   copy-and-paste what `network.sh` prints at the end)
   - `server.sh`: although it's mostly automated, the specification of the range
   is fixed from address 2 to address 5 in the range

- after the server installation, you can log in the server and find a working,
  compiled repository for [iPXE][] in `/home/vagrant/ipxe`. If you generate a
  new image, you should then move/copy it to `/var/tftproot/ipxe` (see
  `pxerv/install.sh` for details);

- the default `/var/tftproot/ipxe` is compiled to chain the request to
  `/var/www/local/htdocs/http.ipxe`. You can modify that file to avoid
  recompiling [iPXE][] and do your experiments.

### Have Fun! In case...

One thing you might hit is that the initial [PXE][] request fails. If this
happens, please install [VirtualBox][]'s Extension Pack from the [download
page][vbdownload].


[iPXE]: http://ipxe.org/
[VirtualBox]: https://www.virtualbox.org/
[Vagrant]: https://www.vagrantup.com/
[Host-only networking]: https://www.virtualbox.org/manual/ch06.html#network_hostonly
[Alpine Linux]: https://alpinelinux.org/
[tftp-hpa]: https://www.kernel.org/pub/software/network/tftp/
[Busybox]: http://busybox.net/
[udhcpd]: https://en.wikipedia.org/wiki/Udhcpc
[lighttpd]: https://www.lighttpd.net/
[PXE]: https://en.wikipedia.org/wiki/Preboot_Execution_Environment
[vbdownload]: https://www.virtualbox.org/wiki/Downloads
[ipxe-boot-example]: http://boot.ipxe.org/demo/
[initrd.img]: https://gitlab.com/polettix/pxe-poc/blob/master/pxerv/initrd.img
[vmlinuz-3.16.0-rc4]: https://gitlab.com/polettix/pxe-poc/blob/master/pxerv/vmlinuz-3.16.0-rc4
