#!/bin/bash

vagrant up
PORT="$(vagrant port | sed -ne '/22 (guest)/s/.*=> \([0-9]*\).*/\1/p')"

(cd pxerv && tar cvzf - .) >pxerv.tar.gz
scp \
   -o Port="$PORT" \
   -o Compression=yes \
   -o DSAAuthentication=yes \
   -o LogLevel=FATAL \
   -o IdentitiesOnly=yes \
   -o StrictHostKeyChecking=no \
   -o UserKnownHostsFile=/dev/null \
   -i "$PWD/.vagrant/machines/default/virtualbox/private_key" \
   pxerv.tar.gz vagrant@127.0.0.1:
rm pxerv.tar.gz

ssh \
   -o Port="$PORT" \
   -o Compression=yes \
   -o DSAAuthentication=yes \
   -o LogLevel=FATAL \
   -o IdentitiesOnly=yes \
   -o StrictHostKeyChecking=no \
   -o UserKnownHostsFile=/dev/null \
   -i "$PWD/.vagrant/machines/default/virtualbox/private_key" \
   vagrant@127.0.0.1 \
   tar xvzf pxerv.tar.gz

ssh \
   -o Port="$PORT" \
   -o Compression=yes \
   -o DSAAuthentication=yes \
   -o LogLevel=FATAL \
   -o IdentitiesOnly=yes \
   -o StrictHostKeyChecking=no \
   -o UserKnownHostsFile=/dev/null \
   -i "$PWD/.vagrant/machines/default/virtualbox/private_key" \
   vagrant@127.0.0.1 \
   sudo ./install.sh
